const q = require("q")
// test data that represents json return from API call
const testData = [{
            id: 1 ,
            name: "Foo"
        },{
            id: 2 ,
            name: "Bar"
        },{
            id: 3 ,
            name: "Baz"
        }];

// represents get list api call
// @param toBeRejected is a hack to trip the reject/catch blocks
function getList( toBeRejected = false ) {
    return q.Promise( ( resolver , reject ) => {
        if( ! toBeRejected ){
            // resolver(testData); // unthrottled
            setTimeout( ()=> resolver(testData), 500 ); // represent API Call
        } else {
            reject( {
                message: "There was an error!"
            });
        }
    });
}

// represents get by Id api call
function getById( id ) {
    return getList( false )
            .then( results => results.find( result => result.id === id ));
}

 function loadAndMapList( dataPromise ) {
    return dataPromise.then( results => results.map( result => new Model(result) ));
 }
//**
//** EXAMPLES
//** 


//Example 1
// getList( false ).then(( result ) => { 
//     console.log( result ) // should be testData
//  })


// Example 2
// getList( true )
//     .then(( result ) => { 
//         console.log( result ) // wont get here
//     })
//     .catch ( error => console.log("example 2 error", error.message ) )
//     .then( result => console.log("example 2: result is undefined becuase of error", result));

 //Example 3
//  getById( 3 ).then( item => console.log( item ));

 //Example 4
//  getById( 3 )
//     .then( item => {
//         console.log( "Example 4: name ", item.name );
//         item.name = item.name + " changed";
//         return item;
//     })
//     .then( item => console.log( "Example 4: name changed ", item.name ));

 //Example 5
//   getById( 3 )
//     .then( item => console.log( item ))
//     .finally(() => console.log( "example 5: finally block: arguments = " , this.arguments ) );

// Example 6
// getList( true )
//     .then(( result ) => { 
//         console.log( result ) // wont get here
//     })
//     .catch ( error => console.log("example 6 error", error.message ) )
//     .then( result => console.log("example 6: result is undefined becuase of error", result))
//     .finally( () => console.log( "example 6: finally block: arguments = " , this.arguments ) );
    


    //example Real World

    /* - d.base.logic.ts
       loadAndMapList(dataPromise: ng.IPromise<ResourceType[]>): (ResourceType & IDynamicLogicItem<ResourceType>)[] {
        let toReturn: (ResourceType & IDynamicLogicItem<ResourceType>)[] = [];

        toReturn.$resolved = false;
        toReturn.$promise = this.$di.$q((resolve, reject) => {
            dataPromise.then(
                (data) => {
                    let promises: ng.IPromise<ResourceType>[] = [];
                    _.forEach(data, (item: ResourceType) => {
                        // convert each child into a LogicItem
                        let logicItem: IDynamicLogicItem<ResourceType> = this.$$mapItem(item);
                        toReturn.push(logicItem.$asMixed());
                        promises.push(logicItem.$promise);
                    });
                    // resolve the promise
                    this.$di.$q.all(promises).then(() => {
                        toReturn.$resolved = true;
                        resolve(toReturn);
                    }).catch(reject);
                });
        });
        return toReturn;
    }
*/

loadAndMapList(getList(false)).then(result => console.log(result));






// representation of class model
class Model {

    constructor( data ) {
        Object.assign(this, data);
    }

}